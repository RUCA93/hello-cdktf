# Hello CDKTF

Hello curious people!

On this repository you will find a very basic example about how to deploy an AWS Lambda function with an API Gateway, and if you are thinking that you will not be able to do it because you don't have an AWS account, forget about that, there's also included a Docker compose to emulate your own account (￣y▽￣)╭ (more details here <https://github.com/iAlan02/hello-localstack>).
